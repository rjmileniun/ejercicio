# -*- coding: utf-8 -*-

from odoo import models, fields, api

class registro(models.Model):
    _name = 'registro.registro'

    nombre = fields.Char(string="Nombre", required=True)
    horas = fields.Integer(string="Horas", required=True)
    mes = fields.Char(string="Mes", required=True)
    nomina = fields.Integer(string="Nomina", compute="get_total_pay", required=True)

    @api.multi
    def get_total_pay(self):
        print('self')
        print(self)
        for data in self:
            domain = [
                {'nombre', '=', data.nombre}
            ]
            records=self.search(domain)
            Horas=records.mapped('horas')
            data.nomina = sum(Horas)

#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100